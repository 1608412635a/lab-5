// Copyright Epic Games, Inc. All Rights Reserved.


#include "MyProject4GameModeBase.h"
#include "MyCharacter.h"
#include "MyPlayerController.h"

AMyProject4GameModeBase::AMyProject4GameModeBase() {
	PlayerControllerClass = AMyPlayerController::StaticClass();

	static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass((TEXT("Blueprint'/Game/MyMyCharacter.MyMyCharacter_C'")));

	if (pawnBPClass.Object) {
		UClass* pawnBP = (UClass*)pawnBPClass.Object;
		DefaultPawnClass = pawnBP;
	}
	else {
		DefaultPawnClass = AMyCharacter::StaticClass();
	}
}