// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerController.h"
#include "MyCharacter.h"

AMyPlayerController::AMyPlayerController() {

}

void AMyPlayerController::SetupInputComponent() {
	Super::SetupInputComponent();

	InputComponent->BindAxis("Move", this, &AMyPlayerController::move);
	InputComponent->BindAxis("Strafe", this, &AMyPlayerController::strafe);
	InputComponent->BindAction("Interact", IE_Pressed, this, &AMyPlayerController::interactStarted);
	InputComponent->BindAction("Jump", IE_Pressed, this, &AMyPlayerController::jumpStarted);

}

void AMyPlayerController::move(float value) {
	AMyCharacter* character = Cast<AMyCharacter>(this->GetCharacter());

	if (character) {
		character->Move(value);
	}
}

void AMyPlayerController::strafe(float value) {
	AMyCharacter* character = Cast<AMyCharacter>(this->GetCharacter());

	if (character) {
		character->Strafe(value);
	}
}

void AMyPlayerController::interactStarted() {
	AMyCharacter* character = Cast<AMyCharacter>(this->GetCharacter());

	if (character) {
		character->InteractionStarted();
	}
}

void AMyPlayerController::jumpStarted() {
	AMyCharacter* character = Cast<AMyCharacter>(this->GetCharacter());

	if (character) {
		character->JumpStarted();
	}
}