 // Fill out your copyright notice in the Description page of Project Settings.


#include "MyCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "Engine/World.h"
#include "GameFramework/SpringArmComponent.h"

#define OUT

// Sets default values
AMyCharacter::AMyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->TargetArmLength = 400.f;
	CameraBoom->SetRelativeRotation(FRotator(0.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false;
}
// Called when the game starts or when spawned
void AMyCharacter::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AMyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint {
		PlayerViewPointLocation, PlayerViewPointRotation
	};
}


// Called to bind functionality to input
void AMyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

bool AMyCharacter::CanPerformAction(ECharacterActionStateEnum updatedAction) {
	switch (CharacterActionState) {
	case ECharacterActionStateEnum::IDLE:
		return true;
		break;
	case ECharacterActionStateEnum::MOVE:
		if (updatedAction != ECharacterActionStateEnum::INTERACT) {
			return true;
		}
		break;
	case ECharacterActionStateEnum::JUMP:
		if (updatedAction == ECharacterActionStateEnum::IDLE || updatedAction == ECharacterActionStateEnum::MOVE) {
			return true;
		}
		break;
	case ECharacterActionStateEnum::INTERACT:
		return false;
		break;
	}
	return false;
}

void AMyCharacter::ApplyMovement(float scale) {
	AddMovementInput(GetActorForwardVector(), scale);
}

void AMyCharacter::ApplyStrafe(float scale) {
	AddMovementInput(GetActorRightVector(), scale);
}

void AMyCharacter::BeginInteraction() {
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Start Interaction"));
	GetWorld()->GetTimerManager().SetTimer(InteractionTimerHandle, this, &AMyCharacter::EndInteraction, InteractionLength);
}

void AMyCharacter::EndInteraction() {
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Start Interaction"));
	UpdateActionState(ECharacterActionStateEnum::IDLE);
}

void AMyCharacter::UpdateActionState(ECharacterActionStateEnum newAction) {
	if (newAction == ECharacterActionStateEnum::MOVE || newAction == ECharacterActionStateEnum::IDLE) {
		if (FMath::Abs(GetVelocity().Size()) <= 0.01f) {
			CharacterActionState = ECharacterActionStateEnum::IDLE;
		}
		else {
			CharacterActionState = ECharacterActionStateEnum::MOVE;
		}
	}
	else {
		CharacterActionState = newAction;
	}
}


FVector LineTraceEnd = PlayerViewPointLocatioin + PlayerViewPointRotation.Vector() * Reach;

FHitResult Hit;

GetWorld()->LineTraceSingleByObjectType {
	OUT Hit;
	PlayerViewPointLocation;
	LineTraceEnd
}


