// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "MyCharacter.generated.h"


UENUM(BlueprintType)
enum class ECharacterActionStateEnum : uint8 {
	IDLE UMETA(DisplayName = "Idling"),
	MOVE UMETA(DisplayName = "Moving"),
	JUMP UMETA(DisplayName = "Jumping"),
	INTERACT UMETA(DisplayName = "Interacting")
};

UCLASS()
class MYPROJECT4_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyCharacter();

	class USpringArmComponent * GetCameraBoom() const { return CameraBoom; }
	class UCameraComponent* GetCameraComponent() const { return CameraComponent; }



	UFUNCTION(BlueprintImplementableEvent)
		void Move(float value);
	UFUNCTION(BlueprintImplementableEvent)
		void Strafe(float value);
	UFUNCTION(BlueprintImplementableEvent)
		void InteractionStarted();
	UFUNCTION(BlueprintImplementableEvent)
		void JumpStarted();

	UFUNCTION(BlueprintCallable)
		bool CanPerformAction(ECharacterActionStateEnum updatedAction);
	UFUNCTION(BlueprintCallable)
		void ApplyMovement(float scale);
	UFUNCTION(BlueprintCallable)
		void ApplyStrafe(float scale);
	UFUNCTION(BlueprintCallable)
		void BeginInteraction();
	UFUNCTION(BlueprintCallable)
		void EndInteraction();
	UFUNCTION(BlueprintCallable)
		void UpdateActionState(ECharacterActionStateEnum newAction);

protected:

	UPROPERTY(Category = Camera, EditAnywhere, BlueprintReadWrite)
		class UCameraComponent* CameraComponent;

	UPROPERTY(Category = Camera, EditAnywhere, BlueprintReadWrite)
		class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		ECharacterActionStateEnum CharacterActionState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float InteractionLength;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	FTimerHandle InteractionTimerHandle;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
