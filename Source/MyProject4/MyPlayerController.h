// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT4_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AMyPlayerController();

protected:
	virtual void SetupInputComponent() override;

private:
	UFUNCTION()
		void move(float value);
	UFUNCTION()
		void strafe(float value);
	UFUNCTION()
		void interactStarted();
	UFUNCTION()
		void jumpStarted();
};
